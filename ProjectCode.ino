#include <Arduino.h>
#include <Adafruit_Sensor.h>
#include <DHT.h>
#include <DHT_U.h>
#include <WiFi.h>
#include <Firebase_ESP_Client.h>
#include <addons/TokenHelper.h>
#include <time.h>

#define DHTPIN 4
#define DHTTYPE DHT11
#define BUTTON_PIN 14
DHT dht(DHTPIN, DHTTYPE);

#define WIFI_SSID "Walid91"
#define WIFI_PASSWORD "%rn9PI3xi"
#define API_KEY "AIzaSyBWrSb1mgr38_GIzhloGWzBuggih4ymH4U"
#define FIREBASE_PROJECT_ID "maruche-91"
#define USER_EMAIL "waliddadi75@gmail.com"
#define USER_PASSWORD "test123"

FirebaseData fbdo;
FirebaseAuth auth;
FirebaseConfig config;

unsigned long dataMillis = 0;
unsigned long buttonMillis = 0;
int previousButtonState = HIGH;
String idRuche = "TfdwZ5HpHQA4IUnaLCLc";
struct tm timeinfo;

void setup() {
  Serial.begin(115200);
  dht.begin();
  pinMode(BUTTON_PIN, INPUT_PULLUP);

  WiFi.begin(WIFI_SSID, WIFI_PASSWORD);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
  }
  
  configTime(0, 0, "pool.ntp.org");
  delay(1000);
  int retries = 0, maxRetries = 10;
  while (!getLocalTime(&timeinfo) && retries < maxRetries) {
    Serial.println("Failed to get time, retrying");
    delay(1000);
    retries++;
  }

  config.api_key = API_KEY;
  auth.user.email = USER_EMAIL;
  auth.user.password = USER_PASSWORD;

  Firebase.begin(&config, &auth);
  Firebase.reconnectWiFi(true);
}

void loop() {
  if (Firebase.ready() && (millis() - dataMillis > 30000 || dataMillis == 0)) {
    dataMillis = millis();
    sendSensorDataToFirebase();
  }

  int buttonState = digitalRead(BUTTON_PIN);
  if (buttonState != previousButtonState && millis() - buttonMillis > 200) {
    previousButtonState = buttonState;
    buttonMillis = millis();
    sendSensorDataToFirebase();
  }
}

String getFormattedDate() {
  char buf[20];
  if (!getLocalTime(&timeinfo)) {
    Serial.println("Failed to obtain time");
    return "";
  }
  strftime(buf, sizeof(buf), "%FT%T", &timeinfo);
  return String(buf);
}

void sendSensorDataToFirebase() {
  float temperature = dht.readTemperature();
  float humidity = dht.readHumidity();
  String currentTime = getFormattedDate();
  
  FirebaseJson rucheContent;
  rucheContent.set("fields/time_last_data/stringValue", currentTime);
  rucheContent.set("fields/temp/doubleValue", temperature);
  rucheContent.set("fields/humidite/doubleValue", humidity);
  rucheContent.set("fields/couvercle/booleanValue", previousButtonState == LOW);

  String rucheDocumentPath = "Ruche/" + idRuche;

  if (Firebase.Firestore.patchDocument(&fbdo, FIREBASE_PROJECT_ID, "", rucheDocumentPath, rucheContent.raw(), "time_last_data,temp,humidite,couvercle")) {
    Serial.printf("Ruche update ok\n%s\n\n", fbdo.payload().c_str());
  } else {
    Serial.println(fbdo.errorReason());
  }

  FirebaseJson mesureContent;
  mesureContent.set("fields/temp/doubleValue", temperature);
  mesureContent.set("fields/humidite/doubleValue", humidity);
  mesureContent.set("fields/couvercle/booleanValue", previousButtonState == LOW);
  mesureContent.set("fields/date/stringValue", currentTime);
  mesureContent.set("fields/idRuche/stringValue", idRuche);

  String mesureCollectionPath = "Mesure";

  if (Firebase.Firestore.createDocument(&fbdo, FIREBASE_PROJECT_ID, "", mesureCollectionPath, mesureContent.raw())) {
    Serial.printf("Mesure update ok\n%s\n\n", fbdo.payload().c_str());
  } else {
    Serial.println(fbdo.errorReason());
  }
}
